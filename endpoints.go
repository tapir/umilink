package main

import (
	"bytes"
	"fmt"
	"io"
	"net/http"
	"strconv"

	"github.com/avct/uasurfer"
	"github.com/julienschmidt/httprouter"
)

var msg string = "OK"

func fixes(r *http.Request) (prefix string, suffix string) {
	ua := uasurfer.Parse(r.UserAgent())
	switch ua.OS.Name {
	case uasurfer.OSiOS:
		prefix = "youtube://"
		suffix = ""
	case uasurfer.OSAndroid:
		prefix = "intent://"
		suffix = "#Intent;package=com.google.android.youtube;scheme=https;end"
	default:
		prefix = "https://"
		suffix = "&sub_confirmation=1"
	}
	return
}

func defaultGET() httprouter.Handle {
	return func(rw http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		var (
			prefix string
			suffix string
		)
		prefix, suffix = fixes(r)
		http.Redirect(rw, r, prefix+"www.youtube.com/yemekonemli?"+suffix, http.StatusMovedPermanently)
	}
}

func adminGET(rw http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	var (
		alStr string
		alias string
		vidID string
	)

	// remove
	keys, ok := r.URL.Query()["remove"]
	if ok && len(keys) > 0 {
		i, err := strconv.Atoi(keys[0])
		if err != nil {
			msg = err.Error()
		} else {
			err = aliases.remove(i)
			if err != nil {
				msg = err.Error()
			}
		}
		if msg == "OK" {
			msg = "Alias successfully removed"
		}
		http.Redirect(rw, r, "/admin", http.StatusMovedPermanently)
		return
	}

	// add
	keys, ok = r.URL.Query()["alias"]
	if ok && len(keys) > 0 {
		alias = keys[0]
	}
	keys, ok = r.URL.Query()["vidid"]
	if ok && len(keys) > 0 {
		vidID = keys[0]
	}
	if alias != "" && vidID != "" {
		err := aliases.add(alias, vidID)
		if err != nil {
			msg = err.Error()
		}
		if msg == "OK" {
			msg = "Alias successfully added"
		}
		http.Redirect(rw, r, "/admin", http.StatusMovedPermanently)
		return
	}

	// default
	for i, v := range aliases.aliases {
		alStr += fmt.Sprintf(item+"\n", v.Alias, v.VidID, "https://yemekonemli.com/"+v.Alias, strconv.Itoa(i))
	}
	fmt.Fprintf(rw, body, alStr, msg)
	msg = "OK"
}

func aliasGET() httprouter.Handle {
	return func(rw http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		var (
			alias  string
			prefix string
			suffix string
		)

		prefix, suffix = fixes(r)
		alias = ps.ByName("alias")

		switch alias {
		case "admin":
			basicAuth(adminGET, "umilizer", "uMuTY3m3k_")(rw, r, ps)
		case "umit.jpg":
			rw.Header().Set("Content-Type", "image/jpeg")
			io.Copy(rw, bytes.NewReader(umitjpg))
		default:
			for _, v := range aliases.aliases {
				if alias == v.Alias {
					if len(v.VidID) == 11 {
						http.Redirect(rw, r, prefix+"www.youtube.com/watch?v="+v.VidID+suffix, http.StatusMovedPermanently)
					} else {
						http.Redirect(rw, r, prefix+"www.youtube.com/playlist?list="+v.VidID+suffix, http.StatusMovedPermanently)
					}
					return
				}
			}
			http.Redirect(rw, r, prefix+"www.youtube.com/yemekonemli?"+suffix, http.StatusMovedPermanently)
		}
	}
}

func videoGET() httprouter.Handle {
	return func(rw http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		var (
			vidID  string
			alias  string
			prefix string
			suffix string
		)
		prefix, suffix = fixes(r)
		alias = ps.ByName("alias")
		vidID = ps.ByName("vidID")

		switch alias {
		case "v":
			if vidID != "" {
				if len(vidID) == 11 {
					http.Redirect(rw, r, prefix+"www.youtube.com/watch?v="+vidID+suffix, http.StatusMovedPermanently)
				} else {
					http.Redirect(rw, r, prefix+"www.youtube.com/playlist?list="+vidID+suffix, http.StatusMovedPermanently)
				}
			} else {
				http.Redirect(rw, r, prefix+"www.youtube.com/yemekonemli?"+suffix, http.StatusMovedPermanently)
			}
		default:
			http.Redirect(rw, r, prefix+"www.youtube.com/yemekonemli?"+suffix, http.StatusMovedPermanently)
		}
	}
}
