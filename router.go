package main

import (
	"fmt"
	"net/http"
	"runtime/debug"

	"github.com/julienschmidt/httprouter"
)

// Register all endpoints here
func setupRouter() http.Handler {
	router := httprouter.New()

	// Panic handler
	router.PanicHandler = func(rw http.ResponseWriter, r *http.Request, data interface{}) {
		logErr.Printf("Panic caught: %s", data)
		debug.PrintStack()
		fmt.Fprint(rw, nil)
	}

	// Web endpoints
	router.GET("/", defaultGET())
	router.GET("/:alias", aliasGET())
	router.GET("/:alias/:vidID", videoGET())

	return nocache(router)
}
