package main

import (
	"log"
	"net/http"
	"os"
)

var (
	logErr  = log.New(os.Stderr, "", 0)
	aliases *aliasList
	err     error
)

const dbPath = "/root/umilink/db.json"

func main() {
	// Log settings
	logErr.SetFlags(log.LstdFlags | log.Lshortfile)

	// Get aliases
	aliases, err = newAliasList(dbPath)
	if err != nil {
		log.Fatal(err)
	}

	// Schedule disk write
	go aliases.schedule(dbPath)

	// App started
	log.Println("Web service started")

	// Serve
	srv := &http.Server{
		Addr:    ":80",
		Handler: setupRouter(),
	}
	if err = srv.ListenAndServe(); err != nil {
		logErr.Fatal(err)
	}
}
