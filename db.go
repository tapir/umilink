package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"time"
)

type aliasItem struct {
	Alias string `json:"alias"`
	VidID string `json:"vidid"`
}

type aliasList struct {
	aliases    []aliasItem
	fileUpdate bool
}

func newAliasList(path string) (*aliasList, error) {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	as := new(aliasList)
	err = json.Unmarshal(data, &as.aliases)
	if err != nil {
		return nil, err
	}
	return as, nil
}

func (a *aliasList) schedule(path string) {
	ticker := time.NewTicker(3 * time.Second)
	for range ticker.C {
		if a.fileUpdate {
			a.fileUpdate = false
			data, err := json.Marshal(&a.aliases)
			if err != nil {
				logErr.Println(err)
				continue
			}
			err = ioutil.WriteFile(path, data, os.ModePerm)
			if err != nil {
				logErr.Println(err)
			}
		}
	}
}

func (a *aliasList) add(alias, vidID string) error {
	// add to mem
	a.aliases = append([]aliasItem{aliasItem{Alias: alias, VidID: vidID}}, a.aliases...)
	a.fileUpdate = true
	return nil
}

func (a *aliasList) remove(i int) error {
	if i < len(a.aliases) {
		// delete from mem
		a.aliases = append(a.aliases[:i], a.aliases[i+1:]...)
		a.fileUpdate = true
	} else {
		return fmt.Errorf("Can't find alias index: %v", i)
	}
	return nil
}
