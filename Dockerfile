FROM golang:1.14.6-alpine

LABEL maintainer.name="Cosku Bas" \
      maintainer.email="cosku.bas@gmail.com"

# install needed software
RUN apk update && apk upgrade && apk add --no-cache git tzdata build-base

# set timezone to utc
RUN cp /usr/share/zoneinfo/UTC /etc/localtime
RUN echo "UTC" >  /etc/timezone

COPY ./ /root/umilink

# install apiserver
RUN cd /root/umilink && go build

# cleanup
RUN apk del git tzdata build-base
RUN rm -rf /var/cache/apk/*

CMD ["/root/umilink/umit"]
