module umit

go 1.14

require (
	github.com/avct/uasurfer v0.0.0-20191028135549-26b5daa857f1
	github.com/gorilla/csrf v1.7.0
	github.com/julienschmidt/httprouter v1.3.0
)
