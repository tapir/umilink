package main

import (
	"math/rand"
	"net/http"
	"time"
)

type nocacheMiddle struct {
	next http.Handler
}

func (n *nocacheMiddle) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	rw.Header().Set("Cache-Control", "no-cache")
	rw.Header().Set("Etag", `"`+RandString(16)+`"`)
	n.next.ServeHTTP(rw, r)
}

func nocache(next http.Handler) *nocacheMiddle {
	return &nocacheMiddle{next: next}
}

const (
	letterBytes   = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	letterIdxBits = 6                    // 6 bits to represent a letter index
	letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
	letterIdxMax  = 63 / letterIdxBits   // Num of letter indices fitting in 63 bits
)

// RandString creates a random string of given length
func RandString(n int) string {
	var b = make([]byte, n)
	var src = rand.NewSource(time.Now().UnixNano())
	for i, cache, remain := n-1, src.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = src.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < len(letterBytes) {
			b[i] = letterBytes[idx]
			i--
		}
		cache >>= letterIdxBits
		remain--
	}
	return string(b)
}
