package main

const item = `
<tr>
<td>%s</td>
<td>%s</td>
<td>%s</td>
<td><a href="/admin?remove=%s">Delete</a></td>
</tr>`

const body = `
<!DOCTYPE html>
<html>
<head>
	<script>
	window.addEventListener("DOMContentLoaded", function () {
		var form = document.getElementById("form-add");
		document.getElementById("link-add").addEventListener("click", function () {
			form.submit();
		});
	});
	</script>
	<style>
		table, td {
			border: 1px solid black;
			border-collapse: collapse;
			padding-left: 10px;
			padding-right: 10px;
			padding-top: 5px;
			padding-bottom: 5px;
		}
	</style>
</head>
<body>
	<table style="border: 0px">
		<tbody>
			<tr>
			<td style="border: 0px"><img src="/umit.jpg" alt="umipic" width="24" height="24" /></td>
			<td style="border: 0px"><h3>UmiLink v1.0</h3></td>
			<td style="border: 0px"><img src="/umit.jpg" alt="umipic" width="24" height="24" /></td>
			</tr>
		</tbody>
	</table>
	<form id="form-add" method="get" action="/admin">
	<table>
		<tbody>
			<tr>
				<td><p><strong>Alias</strong></p></td>
				<td><strong>Video or List ID</strong></td>
				<td><strong>URL</strong></td>
				<td><strong>Action</strong></td>
			</tr>
			<tr>
				<td><input type="text" id="alias" name="alias"></td>
				<td><input type="text" id="vidid" name="vidid"></td>
				<td></td>
				<td><a id="link-add" href="#">Add</a></td>
			</tr>
			%s
		</tbody>
	</table>
	</form>
<p><strong>Status: </strong>%s</p>
</body>
</html>`
